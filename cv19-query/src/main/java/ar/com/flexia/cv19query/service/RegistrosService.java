package ar.com.flexia.cv19query.service;


import ar.com.flexia.cv19query.model.entity.Registro;

import java.time.LocalDate;
import java.util.List;


public interface RegistrosService {
    public Long getCantidadCasos(LocalDate desde, LocalDate hasta, String provincia, String municipio);
    public List<Registro> getUltimosCasos(Long cantidadCasos);
}
