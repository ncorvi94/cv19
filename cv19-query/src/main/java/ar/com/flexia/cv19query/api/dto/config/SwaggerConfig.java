package ar.com.flexia.cv19query.api.dto.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI() {

        Contact contact = new Contact()
                .name("Flexia S.A.S.")
                .email("info@flexia.com.ar")
                .url("https://flexia.com.ar");

        Info info = new Info()
                .title("users")
                .description("cv19-query API")
                .version("v1.0")
                .contact(contact);

        Components components = new Components();

        return new OpenAPI().info(info).components(components);
    }
}

