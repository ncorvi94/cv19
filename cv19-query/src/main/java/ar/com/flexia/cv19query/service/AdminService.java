package ar.com.flexia.cv19query.service;


import ar.com.flexia.cv19query.api.dto.Credentials;
import ar.com.flexia.cv19query.api.dto.Session;

public interface AdminService {
    public Session login(Credentials credentials);
}
