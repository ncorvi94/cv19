package ar.com.flexia.cv19query.api.controller;

import ar.com.flexia.cv19query.model.entity.Registro;
import ar.com.flexia.cv19query.service.RegistrosService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/registros")
public class RegistroController {

    @Autowired
    private RegistrosService registrosService;

    @Operation(summary = "consultar cantidad de casos", description =
            "devuelve la cantidad de casos que se registraron en un determinado lapso de tiempo." +
                    "\nDe manera opcional se pueden agregar como parametros provincia y municipio." +
                    "\nLas fechas deben respetar el siguiente formato: YYYY-MM-DD." +
                    "\nLos nombres de las provincias/municipios deben ir separados por un - (guion medio)." +
                    "\nEjemplo: .../2021-01-30/2022-01-30?provincia=buenos-aires&municipio=san-isidro")
    @GetMapping("/{fechaDesde}/{fechaHasta}")
    @Secured("ADMIN")
    public Long getCantidadCasos(@PathVariable("fechaDesde") String fechaDesde,
                                    @PathVariable("fechaHasta") String fechaHasta,
                                 @RequestParam(name = "provincia", required = false) String provincia,
                                 @RequestParam(name = "municipio", required = false) String municipio){
        LocalDate fechaDesdeParsed = LocalDate.parse(fechaDesde);
        LocalDate fechaHastaParsed = LocalDate.parse(fechaHasta);
        return registrosService.getCantidadCasos(fechaDesdeParsed,fechaHastaParsed, provincia, municipio);
    }

    @Secured("ADMIN")
    @GetMapping("/ultimos-casos/{cantidadCasos}")
    @Operation(summary = "consultar los ultimos N casos")
    public List<Registro> getUltimosCasos(@PathVariable("cantidadCasos") Long cantidadCasos){
        return registrosService.getUltimosCasos(cantidadCasos);
    }
}
