package ar.com.flexia.cv19query.service.impl;

import ar.com.flexia.cv19query.model.entity.Registro;
import ar.com.flexia.cv19query.model.repository.RegistroRepository;
import ar.com.flexia.cv19query.service.RegistrosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Character.valueOf;

@Service
public class RegistrosServiceImpl implements RegistrosService {

    @Autowired
    private RegistroRepository registroRepository;

    public String camelCaseToTitleCase(String string){
        if (string == null) {
            return null;
        }
        String result = "";
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '-'){
                result = result + ' ';
            }else{
                result = result + string.charAt(i);
            }
        }
        return result;
    }

    @Override
    public Long getCantidadCasos(LocalDate desde, LocalDate hasta, String provincia, String municipio) {


        String provinciaTitleCase = camelCaseToTitleCase(provincia);
        String municipioTitleCase = camelCaseToTitleCase(municipio);

        return registroRepository.findAll().stream()
                .filter(registro -> registro.getFechaDeteccion().isEqual(desde) || registro.getFechaDeteccion().isAfter(desde))
                .filter(registro -> registro.getFechaDeteccion().isEqual(hasta) || registro.getFechaDeteccion().isBefore(hasta))
                .filter(registro -> provinciaTitleCase == null || registro.getProvincia().equalsIgnoreCase(provinciaTitleCase))
                .filter(registro -> municipioTitleCase == null || registro.getMunicipio().equalsIgnoreCase(municipioTitleCase))
                .count();
    }

    @Override
    public List<Registro> getUltimosCasos(Long cantidadCasos) {
        return registroRepository.findAll().stream()
                .sorted(Comparator.comparing(Registro::getFechaDeteccion).reversed())
                .limit(cantidadCasos)
                .collect(Collectors.toList());
    }


}
