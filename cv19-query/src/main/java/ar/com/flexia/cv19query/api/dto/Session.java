package ar.com.flexia.cv19query.api.dto;


import ar.com.flexia.cv19query.model.entity.Admin;


public class Session {

	private String token;
	
	private Admin admin;

	/**
	 * @param token
	 * @param admin
	 */
	public Session(String token, Admin admin) {
		super();
		this.token = token;
		this.admin = admin;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the user
	 */
	public Admin getUser() {
		return admin;
	}
}
