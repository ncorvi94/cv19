package ar.com.flexia.cv19query.service;

import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.List;

public interface JWTService {

	/**
	 * Issue a JWT for the given subject and authorities
	 * @param subject
	 * @param email
	 * @param authorities
	 * @return JWT string with configured prefix already appended
	 */
	public String issueToken(Long subject, String username, List<String> authorities);
	
	/**
	 * Verify JWT
	 * @param jwt
	 * @return decoded token
	 */
	public DecodedJWT verify(String jwt);
}
