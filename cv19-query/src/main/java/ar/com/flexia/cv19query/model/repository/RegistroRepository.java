package ar.com.flexia.cv19query.model.repository;


import ar.com.flexia.cv19query.model.entity.Registro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegistroRepository extends JpaRepository<Registro,Long> {

}
