package ar.com.flexia.cv19query.api.controller;

import ar.com.flexia.cv19query.api.dto.Credentials;
import ar.com.flexia.cv19query.api.dto.Session;
import ar.com.flexia.cv19query.model.entity.Admin;
import ar.com.flexia.cv19query.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AdminService adminService;

    @PostMapping("/login")
    public Session login(@RequestBody Credentials admin){
        return adminService.login(admin);
    }
}
