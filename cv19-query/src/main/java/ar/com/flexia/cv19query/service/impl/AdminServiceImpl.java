package ar.com.flexia.cv19query.service.impl;

import ar.com.flexia.cv19query.api.dto.Session;
import ar.com.flexia.cv19query.model.entity.Admin;
import ar.com.flexia.cv19query.model.entity.UserProfile;
import ar.com.flexia.cv19query.model.repository.AdminRepository;
import ar.com.flexia.cv19query.service.AdminService;
import ar.com.flexia.cv19query.service.JWTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ar.com.flexia.cv19query.api.dto.Credentials;

import java.util.Arrays;

@Service
public class AdminServiceImpl implements AdminService {

    private PasswordEncoder passwordEncoder;
    private AdminRepository adminRepository;
    private JWTService jwtService;

    public AdminServiceImpl(PasswordEncoder passwordEncoder, AdminRepository adminRepository, JWTService jwtService) {
        this.passwordEncoder = passwordEncoder;
        this.adminRepository = adminRepository;
        this.jwtService = jwtService;

        if(this.adminRepository.count() == 0){
            Admin admin = new Admin("admin", passwordEncoder.encode("admin"));
            adminRepository.save(admin);
        }



    }

    @Override
    public Session login(Credentials credentials) {
        Admin admin = adminRepository.findByUsername(credentials.getUsername())
                    .orElseThrow(() -> new BadCredentialsException("usuario o clave invalidos"));
        if(!passwordEncoder.matches(credentials.getPassword(), admin.getPassword())){
            throw new BadCredentialsException("usuario o clave invalidos");
        }

        String token = jwtService.issueToken(admin.getId(),admin.getUsername(), Arrays.asList(UserProfile.ADMIN.name()));
        return new Session(token, admin);

    }
}
