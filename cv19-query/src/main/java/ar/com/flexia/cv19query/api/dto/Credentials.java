package ar.com.flexia.cv19query.api.dto;

public class Credentials {

	private String username;
	private String password;
	
	/**
	 * @return the email
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
}
