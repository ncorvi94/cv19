package ar.com.flexia.cv19load.service;



import ar.com.flexia.cv19load.api.dto.Credentials;
import ar.com.flexia.cv19load.api.dto.Session;
import ar.com.flexia.cv19load.model.entity.User;

import java.util.List;

public interface UserService {
    public Session login(Credentials credentials);
    public void createUser(User user);


}
