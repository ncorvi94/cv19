package ar.com.flexia.cv19load.api.dto.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase de configuración de seguridad que usa Spring Security
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	private List<String> corsAllowedOrigins;
	
	private UserDetailsService userDetailsService;
	
	private PasswordEncoder passwordEncoder;
	
	/**
	 * @param corsAllowedOrigins
	 * @param userDetailsService
	 * @param passwordEncoder
	 */
	public SecurityConfig(@Value("#{'${cors.allowed-origins:*}'.split(',')}") List<String> corsAllowedOrigins,
                          UserDetailsService userDetailsService,
                          PasswordEncoder passwordEncoder) {
		super();
		this.corsAllowedOrigins = corsAllowedOrigins;
		this.userDetailsService = userDetailsService;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.cors()
			.and().csrf().disable()
			.headers().frameOptions().sameOrigin() // for local h2
			.and().authorizeRequests()
				.antMatchers("/h2", "/h2/*").permitAll()
				.antMatchers("/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
				// business rules
				.antMatchers(HttpMethod.POST, "/auth/login").permitAll()
			.anyRequest().authenticated()
			.and().addFilter(authorizationFilter())
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}
	
	/**
	 * @return authrozation filter
	 * @throws Exception
	 */
	@Bean
	public JWTAuthorizationFilter authorizationFilter() throws Exception {
		return new JWTAuthorizationFilter(this.authenticationManager());
	}
	
	@Bean
	public GrantedAuthorityDefaults grantedAuthorityDefaults() {
	    return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
	}
	
	/**
	 * @return CORS configuration
	 */
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		List<String> allowedMethods = new ArrayList<>();
		allowedMethods.add(CorsConfiguration.ALL);
		CorsConfiguration corsConfig = new CorsConfiguration().applyPermitDefaultValues();
		corsConfig.setAllowedMethods(allowedMethods);
		corsConfig.addExposedHeader(JWTConfig.HEADER_NAME);
		corsConfig.setAllowedOrigins(corsAllowedOrigins);
		corsConfig.setAllowCredentials(true);
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", corsConfig);
		return source;
	}
}
