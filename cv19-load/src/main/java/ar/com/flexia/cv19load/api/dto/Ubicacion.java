package ar.com.flexia.cv19load.api.dto;

import ar.com.flexia.cv19load.model.entity.Paciente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


public class Ubicacion {

    private String provincia;

    private String municipio;

    private Paciente paciente;

    public Ubicacion(String provincia, String municipio, Paciente paciente) {
        this.provincia = provincia;
        this.municipio = municipio;
        this.paciente = paciente;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    @Override
    public String toString() {
        return "Ubicacion{" +
                "provincia='" + provincia + '\'' +
                ", municipio='" + municipio + '\'' +
                ", paciente=" + paciente +
                '}';
    }
}
