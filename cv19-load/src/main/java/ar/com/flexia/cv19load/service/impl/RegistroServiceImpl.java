package ar.com.flexia.cv19load.service.impl;

import ar.com.flexia.cv19load.api.dto.NuevoRegistro;
import ar.com.flexia.cv19load.model.entity.Registro;
import ar.com.flexia.cv19load.model.repository.PacienteRepository;
import ar.com.flexia.cv19load.model.repository.RegistroRepository;
import ar.com.flexia.cv19load.service.RegistroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RegistroServiceImpl implements RegistroService {


    private RegistroRepository registroRepository;
    private PacienteRepository pacienteRepository;


    public RegistroServiceImpl(RegistroRepository registroRepository, PacienteRepository pacienteRepository) {
        this.registroRepository = registroRepository;
        this.pacienteRepository = pacienteRepository;


    }


    @Override
    @Transactional
    public void crearRegistro(NuevoRegistro nuevoRegistro) {
        Registro registro = new Registro(
                nuevoRegistro.getCliente(),
                nuevoRegistro.getDeteccion(),
                nuevoRegistro.getUbicacion().getProvincia(),
                nuevoRegistro.getUbicacion().getMunicipio(),
                nuevoRegistro.getPaciente()

        );
        registroRepository.save(registro);
    }
}
