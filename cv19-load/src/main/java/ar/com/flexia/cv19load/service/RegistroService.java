package ar.com.flexia.cv19load.service;


import ar.com.flexia.cv19load.api.dto.NuevoRegistro;

public interface RegistroService {
    public void crearRegistro(NuevoRegistro nuevoRegistro);
}
