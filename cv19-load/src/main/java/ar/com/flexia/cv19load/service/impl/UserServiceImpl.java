package ar.com.flexia.cv19load.service.impl;

import ar.com.flexia.cv19load.api.dto.Credentials;
import ar.com.flexia.cv19load.api.dto.Session;
import ar.com.flexia.cv19load.model.entity.User;
import ar.com.flexia.cv19load.model.entity.UserProfile;
import ar.com.flexia.cv19load.model.repository.UserRepository;
import ar.com.flexia.cv19load.service.JWTService;

import ar.com.flexia.cv19load.service.UserService;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;
    private final UserRepository userRepository;


    public UserServiceImpl(PasswordEncoder passwordEncoder, JWTService jwtService, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        this.userRepository = userRepository;

        if (0 == this.userRepository.count()) {
            User user = new User("user", passwordEncoder.encode("user"));
            userRepository.save(user);
        }
    }

    public Session login (Credentials credentials){
        User user = userRepository.findByUsername(credentials.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("usuario o clave invalidos"));

        if(!passwordEncoder.matches(credentials.getPassword(), user.getPassword())){
            throw new BadCredentialsException("Usuario o clave invalidos");
        }

        String token = jwtService.issueToken(user.getId(),user.getUsername(), Arrays.asList(UserProfile.USER.name()));
        return new Session(token, user);
    }

    public void createUser(User user){
        try{
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
        }catch(IllegalArgumentException e) {
            //El campo nombre del user es Unique
            e.printStackTrace();
        }
    }








}
