package ar.com.flexia.cv19load.api.dto;

import ar.com.flexia.cv19load.model.entity.Paciente;

import java.time.LocalDate;


public class NuevoRegistro {
    private String cliente;

    private LocalDate deteccion;

    private Ubicacion ubicacion;

    private Paciente paciente;

    public NuevoRegistro(String cliente, LocalDate deteccion, Ubicacion ubicacion, Paciente paciente) {
        this.cliente = cliente;
        this.deteccion = deteccion;
        this.ubicacion = ubicacion;
        this.paciente = paciente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public LocalDate getDeteccion() {
        return deteccion;
    }

    public void setDeteccion(LocalDate deteccion) {
        this.deteccion = deteccion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    @Override
    public String toString() {
        return "NuevoRegistro{" +
                "cliente='" + cliente + '\'' +
                ", fechaDeteccion=" + deteccion +
                ", ubicacion=" + ubicacion +
                ", paciente=" + paciente +
                '}';
    }
}
