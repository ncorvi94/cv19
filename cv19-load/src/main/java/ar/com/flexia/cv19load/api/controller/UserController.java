package ar.com.flexia.cv19load.api.controller;

import ar.com.flexia.cv19load.model.entity.User;
import ar.com.flexia.cv19load.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/users")
    @Secured("ADMIN")
    public void createUser(@RequestBody User user){
        userService.createUser(user);
    }
}
