package ar.com.flexia.cv19load.api.controller;

import ar.com.flexia.cv19load.api.dto.NuevoRegistro;
import ar.com.flexia.cv19load.service.RegistroService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/registros")
public class RegistroController {


    @Autowired
    private RegistroService registroService;

    @PostMapping
    @Operation(summary = "crea un nuevo registro")
    public void crearRegistro(@RequestBody NuevoRegistro nuevoRegistro){
        registroService.crearRegistro(nuevoRegistro);
    }
}
