package ar.com.flexia.cv19load.api.controller;
import ar.com.flexia.cv19load.api.dto.Credentials;
import ar.com.flexia.cv19load.api.dto.Session;
import ar.com.flexia.cv19load.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public Session login(@RequestBody Credentials credentials){
        return userService.login(credentials);
    }

}
