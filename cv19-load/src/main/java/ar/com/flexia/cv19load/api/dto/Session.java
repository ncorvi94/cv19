package ar.com.flexia.cv19load.api.dto;

import ar.com.flexia.cv19load.model.entity.User;


public class Session {

	private String token;
	
	private User user;

	/**
	 * @param token
	 * @param user
	 */
	public Session(String token, User user) {
		super();
		this.token = token;
		this.user = user;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
}
