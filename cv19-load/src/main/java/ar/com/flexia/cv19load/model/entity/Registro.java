package ar.com.flexia.cv19load.model.entity;

import ar.com.flexia.cv19load.api.dto.Ubicacion;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Registro {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String cliente;

    @Column(name = "deteccion")
    private LocalDate fechaDeteccion;

    @Column
    private String provincia;

    @Column
    private String municipio;

    @OneToOne(cascade = CascadeType.ALL)
    private Paciente paciente;

    public Registro(String cliente, LocalDate fechaDeteccion, String provincia, String municipio, Paciente paciente) {
        this.cliente = cliente;
        this.fechaDeteccion = fechaDeteccion;
        this.provincia = provincia;
        this.municipio = municipio;
        this.paciente = paciente;
    }

    protected Registro() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public LocalDate getFechaDeteccion() {
        return fechaDeteccion;
    }

    public void setFechaDeteccion(LocalDate fechaDeteccion) {
        this.fechaDeteccion = fechaDeteccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }



    @Override
    public String toString() {
        return "Registro{" +
                "id=" + id +
                ", cliente='" + cliente + '\'' +
                ", fechaDeteccion=" + fechaDeteccion +
                ", provincia='" + provincia + '\'' +
                ", municipio='" + municipio + '\'' +
                ", paciente=" + paciente +
                '}';
    }
}
